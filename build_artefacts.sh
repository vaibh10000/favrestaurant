#!/bin/bash

#
# Generates Android build
#

echo -e $PWD

ASSEMBLE="assemble"
ENVIRONMENT=$1
BUILDTYPE=$2
BUILDNAME=$3

command=$ASSEMBLE$ENVIRONMENT$BUILDTYPE

echo $command

./gradlew --info clean $command

# Check if android build generated
if [ $? -ne 0 ]; then
  echo -e $RED $BOLD
  echo -e "Vaibhav Could not generate Android build."
  echo -e $NO_COLOR $REGULAR
  exit 1
else
  echo -e $RED $BOLD
  echo -e "Vaibhav generate Android build."
  echo -e $NO_COLOR $REGULAR
fi

# Copy apk
if [ ! -e ./app/build/apks ]; then
  mkdir ./app/build/apks
fi

# Check if android build generated
if [ $? -ne 0 ]; then
  echo -e $RED $BOLD
  echo -e "Vaibhav folder is not created."
  echo -e $NO_COLOR $REGULAR
  exit 1
else
  echo -e $RED $BOLD
  echo -e "Vaibhav folder is created."
  echo -e $NO_COLOR $REGULAR
fi

#cp ./app/build/outputs/apk/debug/*.apk ./app/build/apks/$BUILDNAME

# Check if android builds copied
if [ $? -ne 0 ]; then
  echo -e $RED $BOLD
  echo -e "Could not copy Android build."
  echo -e $NO_COLOR $REGULAR
else
  echo -e $GREEN $BOLD
  echo -e "-----------------------------------------------------------"
  echo -e "APK Copied to `pwd`/app/build/apks"
  echo -e "-----------------------------------------------------------"
  echo -e $NO_COLOR $REGULAR
fi
