package com.example.myrestaurants.base

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.plus
import org.koin.core.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent {
    protected val mainScopeScope by lazy {
        viewModelScope + CoroutineExceptionHandler { context, throwable ->
            handleException(throwable)
        } + CoroutineName(this::class.java.simpleName)
    }
    protected val ioScope by lazy {
        viewModelScope + CoroutineExceptionHandler { context, throwable ->
            handleIOException(throwable)
        } + CoroutineName(this::class.java.simpleName)
    }

    open fun handleException(throwable: Throwable) {

    }

    open fun handleIOException(throwable: Throwable) {

    }

}
